TARGET=mrtcdemo

CPP=g++
CPPFLAGS= -g -c -frtti -std=c++14

INCLUDE = -Iinclude -Idemo
LDFLAGS = -Llib -lmrtclinux -lavformat -lavcodec -lavutil -lswresample -Wl,-rpath lib 
LDFLAGS += -lm -ldl -lpthread

CPPFILES := $(wildcard src/*.cpp)
CPPOBJS := $(patsubst %cpp, %o, $(CPPFILES))
%.o:%.cpp
	$(CPP) $(CPPFLAGS) $(INCLUDE) $< -o $@

.PHONY: all

all:$(TARGET)
	
$(TARGET): $(CPPOBJS)
	$(CPP) $(CPPOBJS) -o $@ $(LDFLAGS)

clean:
	rm -rf src/*.o
	rm -rf $(TARGET)
