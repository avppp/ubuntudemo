#ifndef MrtcLinux_H
#define MrtcLinux_H
#include "mrtccommon.h"
#include <list>
namespace mrtclinux {
    /**
    *  根据底层网络状态的反馈情况以及订阅方视频解码的情况,
    *  实时调节上层业务方编码器的输出帧率和码率
    *  业务方可以根据编码器的特性，尽可能的做出对应的调整
     **/
    struct MRtcEncoderQosParams {
        int target_bps;
        int target_fps;
    };
    /**
     *  会话监听器，可以侦听会话中产生的一些事件，比如发布成功，订阅成功等
     *  当有新的发布者加入或者离开会话，会得到通知，告知该发布者的的一些信息，包括使用的流ID，用户ID等
     *  如果订阅了某个发布者的流数据，那么还会收到其相应的音视频数据
     **/
    struct MRtcSessionListener {
        MRtcSessionListener() {};
        virtual ~MRtcSessionListener(){};
        virtual void OnSessionEvent(RtcEvent event) = 0;
        virtual void OnNewPublisher(StreamInfo info) = 0;
        virtual void OnStreamGone(StreamInfo info) = 0;
        virtual void OnNewSubscriber(StreamInfo info) = 0;
        virtual void OnMixedAudio(const RtcAudioData& audio){}                        //混音后的音频数据,该接口保留,两人场景下没有用到
        virtual void OnAudio(const RtcAudioData& audio,const std::string& streamId) = 0;
        virtual void OnVideo(const RtcVideoData& video,const std::string& streamId) = 0;
        virtual void OnText(const RtcTextData& text) = 0;

        //H264-NALU对接的场景下，需要反馈一些信息 给到编码器做实时调节
        virtual void OnKeyFrameRequest() {}
        virtual void OnEncoderQosRequest(const MRtcEncoderQosParams& params) {}

        //通知有人进入房间，主要是确保2人都在房间以后，再启动录制，业务不需要录制则不需要关注该回调
        virtual void OnNewJoiner(UserInfo user) {}

        //通知有人离开房间, 离开的人可以是某个观众，也可以是某个发布者
        virtual void OnUserGone(UserInfo user) {}
    };
    /**
     *  会话控制，每次通话对应一个会话
     *  用于创建房间，加入房间，发布，订阅等操作，只有加入或者创建房间成功，才会分配合法的sessionId
     *  对于房间创建者，典型流程是: CreateRoom -> Publish -> FeedAudio/FeedVideo -> LeaveRoom，后续有人加入再做 Subscribe
     *  对于房间加入者，典型流程是: JoinRoom -> Subscribe -> LeaveRoom，当然,JoinRoom以后也可以做Publish
     *  另外还支持通过SendMessage接口发送文本消息到对端，peers存放接收方的uid列表
     **/
    struct MRtcSession {
        MRtcSession() {};
        virtual ~MRtcSession() {};
        virtual void CreateRoom(const CreatRoomParam& createParam) = 0;
        virtual void JoinRoom(const JoinRoomParam& joinParam) = 0;
        virtual void Publish(const PublishParam& pubParam) = 0;
        virtual void Subscribe(const SubscribeParam& subParam) = 0;
        virtual void FeedAudio(const RtcAudioData& audio) = 0;
        virtual void FeedVideo(const RtcVideoData& video) = 0;
        virtual void SendText(const std::string& text,const std::list<std::string> peers) = 0;
        virtual void SendData(const std::string& data) = 0;
        virtual void LeaveRoom() = 0;
        virtual std::string GetSessionId() = 0;
        virtual void ClearAudio() = 0;
        virtual int32_t AudioQueueSize() = 0;
        virtual void StartRecord(const std::string& param="") = 0;
    };
    /**
     *  引擎监听器，监听一些引擎相关的事件，比如初始化成功，失败等等
     **/
    struct MRtcEngineListener {
        MRtcEngineListener() {};
        virtual ~MRtcEngineListener(){};
        virtual void OnEngineEvent(RtcEvent event) = 0;
    };
    /**
     *  音视频引擎，用于创建，销毁会话
     **/
    struct MRtcEngine {
        static MRtcEngine* Create(MRtcEngineListener* engineListener);
        virtual int32_t Init(const MRtcEngineInitParam& param) = 0;
        virtual void Destroy() = 0;
        virtual MRtcSession* CreateSession(MRtcSessionListener* sessionListener) = 0;
        virtual void DestroySession(MRtcSession* session) = 0;
        virtual std::string GetVersion() = 0;
    };
}
#endif