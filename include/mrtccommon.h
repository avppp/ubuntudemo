#ifndef MrtcCommon_H
#define MrtcCommon_H
#include <unistd.h>
#include <string>
namespace mrtclinux {
    enum ErrorCode
    {
        SUCCESS = 0,
        SYSTEM_INNER_ERROR = -1,
        REQ_PARAM_ILLEGAL = -2,
        SIGN_CHECK_FAIL = -3,
        CREATE_ROOM_FAIL = -4,
        JOIN_ROOM_FAIL = -5,
        EXIT_ROOM_FAIL = -6,
        SESSION_NOT_FOUND = -7,
        PUBLISH_FAIL = -8,
        SUBSCRIBE_FAIL = -9,
        REPOST_SDP_ICE_FAIL = -10,
        UNPUBLISH_FAIL = -11,
        UNSUBSCRIBE_FAIL = -12,
        INVITE_JOIN_ROOM_FAIL = -13,
        REPLY_INVITE_JOIN_ROOM_FAIL = -14,
        REPOST_MSG_FAIL = -15,
        START_RECORD_FAIL = -16,
        STOP_RECORD_FAIL = -17,
        ROOM_NOT_FOUND = -18,
        ILLEGAL_REQUEST = -19,
        ICE_LINK_FAILED = -20,
    };
    enum EventType {
        INIT = 0,
        WSS_LINK,                         //信令链路
        CREATE_ROOM,
        JOIN_ROOM,
        PUBLISH,
        SUBSCRIBE,
        EXIT_ROOM,
        ICE_LINK,                        //webrtc数据链路
        START_RECORD                     //录制启动事件
    };
    enum EngineType {
        ENGINE_ALIPAY = 0,               //支付宝内的视频通话,通话双方走蚂蚁SFU
        ENGINE_ALIYUN = 1,               //通话双方至少有一方对接阿里云视频服务器
        ENGINE_P2P = 2                   //支付宝内的视频通话,通话双方直连，不走蚂蚁SFU
    };
    struct RtcEvent {
        std::string uid;                 //和某个用户关联的事件
        EventType type;                  //事件分类
        int32_t code;                    //小于0表示错误,等于0表示正常
        std::string ext = "";            //扩展字段,比如某个用户订阅A/B/C三路流,该字段可以设置为流ID,告知每路流的订阅结果
    };
    struct CreatRoomParam {
        std::string uid;                 //用户ID,唯一标识房间里的某个用户
        std::string sign;                //房间服务器需要验证该通话业务的合法性
        std::string bizName;             //业务类型,比如bank(银行类业务)、stock(证券类业务)
        std::string subBiz;              //子业务类型,比如上海银行,平安证券等
        std::string workspaceId;         //MPaaS类业务需要填写，其他业务不需要该字段
        bool autoSubscribe;              //当房间里面有新人进入，是否自动订阅
        EngineType engine = ENGINE_ALIPAY;               //引擎类型，决定通话双方是否使用SFU，以及SFU的归属
        std::string ext;                 //留作扩展
    };
    struct JoinRoomParam {
        std::string roomId;
        std::string uid;
        std::string sign;
        std::string bizName;
        std::string subBiz;
        std::string token;               //加入房间的凭证
        std::string workspaceId;         //MPaaS类业务需要填写，其他业务不需要该字段
        bool autoSubscribe;              //是否自动订阅房间里面发布者发布的音视频流
        EngineType engine = ENGINE_ALIPAY;
        std::string ext;                 //留作扩展
    };
    struct PublishParam {
        PublishParam():enableVideo(false),enableAudio(true),tag(""){}
        bool enableVideo;                //是否发布本地视频到视频服务器
        bool enableAudio;                //是否发布本地音频到视频服务器
        std::string tag;                 //针对这路流的唯一标识，业务自定义，默认为空
    };
    struct SubscribeParam {
        SubscribeParam():enableVideo(false),enableAudio(true){}
        bool enableVideo;               //手动订阅的场景下，是否订阅视频
        bool enableAudio;               //手动订阅的场景下，是否订阅音频
        std::string streamId;           //房间里面的每个发布者都有唯一的一个流标识，这个标识指明去订阅哪个参与者的音视频流
    };
    struct StreamInfo {
        std::string uid;              //流的发布者或者订阅者
        std::string id;               //流ID
    };
    struct UserInfo {
        std::string uid;             //用户ID
        int type;                    //用户类型,比如手机端用户角色，录制角色，语音播报角色，虚拟人角色等等，默认是0，表示普通用户
    };
    enum RtcVideoCodec {
        VIDEO_CODEC_YUV420P,          //视频输入输出是未编码的YUV数据(I420P)
        VIDEO_CODEC_H264,             //视频输入输出是编码好的H264数据
    };

    struct RtcVideoFormat {
        RtcVideoFormat():width(640),height(360),fps(25),kbps(600),videoCodec(VIDEO_CODEC_YUV420P){}
        int32_t width;               //YUV输入场景下,设置视频编码后的视频宽度;H264输入场景下，指明输入的视频宽度
        int32_t height;              //YUV输入场景下,设置视频编码后的视频高度;H264输入场景下，指明输入的视频高度 
        int32_t fps;                 //YUV输入场景下,设置视频编码后的输出帧率;H264输入场景下，指明输入的视频帧率
        int32_t kbps;                //YUV输入场景下,设置视频编码后的输出码率;H264输入场景下，指明输入的视频码率
        RtcVideoCodec videoCodec;
    };

    enum RtcAudioCodec {
        AUDIO_CODEC_PCM,            //音频当前只支持PCM格式的输入和输出
    };

    struct RtcAudioFormat {
        RtcAudioFormat():channels(1),sampleRate(16000),bytesPerSample(2),audioCodec(AUDIO_CODEC_PCM){}
        int32_t            channels;               //音频声道数
        int32_t            sampleRate;             //音频采样率
        int32_t            bytesPerSample;         //单个样本点字节数
        RtcAudioCodec      audioCodec;
    };

    struct RtcVideoData {
        const uint8_t *dataY;      //引擎初始化为VIDEO_FORMAT_YUV420P为Y分量缓存，其他情况为编码后视频帧缓存地址
        const uint8_t *dataU;      //引擎初始化为VIDEO_FORMAT_YUV420P时必须,其他情况无需关注
        const uint8_t *dataV;      //引擎初始化为VIDEO_FORMAT_YUV420P时必须,其他情况无需关注
        int32_t strideY;           //引擎初始化为VIDEO_FORMAT_YUV420P时必须,其他情况无需关注
        int32_t strideU;           //引擎初始化为VIDEO_FORMAT_YUV420P时必须,其他情况无需关注
        int32_t strideV;           //引擎初始化为VIDEO_FORMAT_YUV420P时必须,其他情况无需关注
        int32_t length;            //视频数据长度,引擎初始化为VIDEO_CODEC_H264必须,其他情况无需关注
        int32_t width;             //视频宽
        int32_t height;            //视频高
        int64_t timestamp;         //视频时间戳,必须
    };

    struct RtcAudioData {
        const uint8_t *data;           //必须，音频数据缓存
        int32_t length;                //必须，音频数据长度
        int64_t timestamp;             //必须，音频时间戳
    };

    struct RtcTextData {
        std::string msg;             //文本的内容
        std::string peer;            //文本发送方的ID
    };

    struct MRtcEngineInitParam {
        MRtcEngineInitParam() {
            roomUrl = "wss://artvcroom.alipay.com/ws";
            logLevel = "debug";
            maxSessionNum = 20;
            enableRelay = false;
            enableDataChannel = false;
            externalIp = "";
        }
        std::string roomUrl;             //房间服务器的接入地址
        RtcVideoFormat videoFormat;      //视频输入输出格式
        RtcAudioFormat audioFormat;      //音频输入输出格式
        std::string logLevel;       //日志级别，none|error|warn|info|debug 中的某一个，none表示不打印日志
        int32_t maxSessionNum;      //运行并发的最大会话数量,大于0时配置生效 
        bool enableRelay;           //如果服务器没有公网IP,可以考虑打开该开关，走数据中转模式
        bool enableDataChannel;     //是否开启DataChannel功能
        std::string externalIp;     //p2p模式下，如果服务器配置了外网IP，但是ifconfig不显示外网IP，需要明确配置到externalIp中,让底层识别
    };
}
#endif