#ifndef MrtcYuvFile_H
#define MrtcYuvFile_H

#include "mrtccommon.h"
#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <vector>

namespace mrtclinux {

class YuvFile {
 public:
  YuvFile();
  ~YuvFile();
  void Open(const std::string& filename,const char* mode);
  void Open(const std::string& filename,size_t width,size_t height,const char* mode,bool auto_rewind=true);
  RtcVideoData ReadFrame();
  void WriteFrame(const RtcVideoData& frame);

  void Close();
  bool EndOfFile() const { return end_of_file_; }

  void FastForward(int frames);
  void Rewind();
  bool Rewinded();

 private:
  FILE* yuv_file_;
  size_t width_;
  size_t height_;
  bool end_of_file_;
  bool auto_rewind_;
  bool rewinded_;
  std::vector<fpos_t> frame_positions_;
  size_t read_idx_;
  size_t total_frames_;
};

} 
#endif
