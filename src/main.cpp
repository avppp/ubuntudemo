#include "demo.h"
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <memory>
#include <cstring>

using namespace mrtclinux;
bool running = true;

std::string WorkingDir() {
  char path_buffer[FILENAME_MAX];
  if (!getcwd(path_buffer, sizeof(path_buffer))) {
    fprintf(stderr, "Cannot get current directory!\n");
    return "./";
  } else {
    return std::string(path_buffer) + '/';
  }
}

void mrtcdemo_proc_signals(int sig)
{
  if (sig == SIGPIPE || sig == SIGHUP)
  {
    
  } else if (sig == SIGINT ) {
    running = false;
  }
}

void mrtcdemo_setup_signals()
{
  signal(SIGINT,mrtcdemo_proc_signals);
  signal(SIGHUP, mrtcdemo_proc_signals);
  signal(SIGPIPE, mrtcdemo_proc_signals);
}

int main(int argc, char** argv) {
  mrtcdemo_setup_signals();

  //std::string roomUrl = "wss://artvcroom.alipay.com/ws";                                //生产环境
  std::string roomUrl = "wss://artvcroomdev.dl.alipaydev.com/ws";                         //开发环境
  //std::string roomUrl = "wss://artvcroom-open.test.dl.alipaydev.com/ws";                //测试环境
  //std::string roomUrl = "wss://cn-hangzhou-mrtc.cloud.alipay.com/ws";                   //MPaaS线上环境

  int32_t maxSessionAllowed = 1;
  int32_t sessionTime = 60;
  std::string roomId = "";
  std::string token="123";
  bool mocktts = false;
  bool p2p = false;
  bool dchannel = false;
  std::string eip = "";
  for(int i=0; i < argc ; i++) {
    if(strcmp(argv[i],"-s") == 0) {
      maxSessionAllowed = atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-t") == 0) {
      sessionTime = atoi(argv[i+1]);
    }
    if(strcmp(argv[i],"-r") == 0) {
      roomId = std::string(argv[i+1]);
    }
    if(strcmp(argv[i],"-k") == 0) {
      token = std::string(argv[i+1]);
    }
    if(strcmp(argv[i],"-R") == 0) {
      roomUrl = std::string(argv[i+1]);
    }
    if(strcmp(argv[i],"-m") == 0) {
      mocktts = true;
    }
    if(strcmp(argv[i],"-p") == 0) {
      p2p = true;
    }
    if(strcmp(argv[i],"-d") == 0) {
      dchannel = true;
    }
    if(strcmp(argv[i],"-e") == 0) {
      eip = std::string(argv[i+1]);;
    }     
  }
  std::cout << "Session Allowed:" << maxSessionAllowed << ",life time:" << sessionTime << std::endl;

  RtcVideoFormat video;
  video.fps = 24;
  video.width = 640;
  video.height = 360;
  video.kbps = 600;
  video.videoCodec = VIDEO_CODEC_YUV420P;

  RtcAudioFormat audio;
  audio.bytesPerSample = 2;
  audio.channels = 1;
  audio.sampleRate = 16000;
  audio.audioCodec = AUDIO_CODEC_PCM;
  std::string logLevel = "debug";

  MRtcEngineInitParam param;
  param.roomUrl = roomUrl;
  param.audioFormat = audio;
  param.videoFormat = video;
  param.logLevel = logLevel;
  param.enableDataChannel = dchannel;
  param.enableRelay = false;
  param.externalIp = eip;
  if(param.maxSessionNum < maxSessionAllowed) {
    param.maxSessionNum = maxSessionAllowed;
  }
  
  //对发布和订阅，以及是否发布音频，是否发布视频做精细化控制
  bool doPub = true;
  bool doSub = true;
  bool pubAudio = true;
  bool pubVideo = true;

  //模拟音视频数据的输入,当前是从文件中读取后输入
  AVSourceConfig config = {
                      WorkingDir() + "data/audio.pcm",
                      WorkingDir() + "data/video.yuv",
                      AV_FILE_PCM,
                      AV_FILE_YUV,
                      640,360,24,
                      audio.sampleRate,
                      audio.channels};
  auto demo = std::make_unique<Demo>(param,maxSessionAllowed);
  while(running){
    int32_t randTime = ::rand() % sessionTime;
    std::cout << "randTime " << randTime << std::endl;
    demo->FireNewSession(roomId,token,config,sessionTime-randTime,doPub,doSub,mocktts,pubVideo,pubAudio,p2p);
    sleep(1);
  }
  std::cout << "Bye!" << std::endl;
  return 0;
}
