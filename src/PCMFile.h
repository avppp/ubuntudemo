#ifndef MrtcPcmFile_H
#define MrtcPcmFile_H

#include <stdio.h>
#include <stdlib.h>

#include <string>

namespace mrtclinux {

class PCMFile {
 public:
  PCMFile();
  PCMFile(uint32_t timestamp);
  ~PCMFile();

  void Open(const std::string& filename,
            uint16_t frequency,
            uint32_t channels,
            const char* mode,
            bool auto_rewind = true);

  void Write10MsData(const int16_t* playout_buffer, size_t length_smpls);
  bool Read10MsData(int16_t* record_buffer, size_t length_smpls);

  uint16_t PayloadLength10Ms() const;
  int32_t SamplingFrequency() const;
  void Close();
  bool EndOfFile() const { return end_of_file_; }
  // Moves forward the specified number of 10 ms blocks. If a limit has been set
  // with SetNum10MsBlocksToRead, fast-forwarding does not count towards this
  // limit.
  void FastForward(int num_10ms_blocks);
  void Rewind();
  bool Rewinded();
  void SaveStereo(bool is_stereo = true);
  void ReadStereo(bool is_stereo = true);
  // If set, the reading will stop after the specified number of blocks have
  // been read. When that has happened, EndOfFile() will return true. Calling
  // Rewind() will reset the counter and start over.
  void SetNum10MsBlocksToRead(int value);

 private:
  FILE* pcm_file_;
  uint16_t samples_10ms_;
  int32_t frequency_;
  bool end_of_file_;
  bool auto_rewind_;
  bool rewinded_;
  uint32_t timestamp_;
  bool read_stereo_;
  bool save_stereo_;
  int num_10ms_blocks_to_read_;
  int blocks_read_ = 0;
};

} 
#endif
