#ifndef MrtcDemo_H
#define MrtcDemo_H
#include "mrtclinux.h"
#include "PCMFile.h"
#include "YuvFile.h"
#include "Mp4File.h"
#include <memory>
#include <iostream>
#include <vector>
#include <atomic>
#include <thread>
namespace mrtclinux {
    enum AVFileType {
        AV_FILE_YUV,
        AV_FILE_PCM,
        AV_FILE_H264
    };
    struct AVSourceConfig {
        const std::string audioFile;
        const std::string videoFile;
        AVFileType audioType;
        AVFileType videoType;
        int yuvWidth;
        int yuvHeight;
        int yuvFps;
        int audioSampleRate;
         int audioChannels;
    };
    struct LinuxSession : public MRtcSessionListener,
                                                public AvDataCallback 
    {
        LinuxSession(MRtcEngine* engine,
                                    const std::string& roomId, 
                                    const std::string& token,
                                    const AVSourceConfig& config,
                                    bool doPublish=true,
                                    bool doSubscribe=true,
                                    bool mocktts=false,
                                    bool enableVideo=true,
                                    bool enableAudio=true);
        virtual ~LinuxSession();
        /**
         *  入口函数，连接房间服务器，初始化音视频参数
         * */
        void Start(int uidIndex,int ridIndex,bool enableP2P);

        /**
         * 监听一次通话过程中发生的事件，包括创建，加入，发布，订阅，离开等事件
         * */
        void OnSessionEvent(RtcEvent event) override ;
        /**
         *  通知上层房间里面有新人发布的流
         *  如果创建房间或者进入房间设置了自动订阅，那么底层会自动订阅该新人的流
         *  否则需要在该函数里面手动订阅
         **/
        void OnNewPublisher(StreamInfo info) override;
        /**
         *  通知上层房间里面有人订阅了某个流
         **/
        void OnNewSubscriber(StreamInfo info) override;
        /**
         *  通知上层房间里面有某路流停止了发布
         *  如果是两人的房间，那么相当于对方退出了，我方这个时候也可以退出
         *  如果是多人的房间，那么相当于某个人退出了，我方可以退出也可以不退出，继续接收房间里面其他人的流
         **/
        void OnStreamGone(StreamInfo info) override;
        /**
         *  收到了来自某路流的音频数据(PCM)
         **/        
        void OnAudio(const RtcAudioData& audio,const std::string& streamId) override;
        /**
         *  多路音频流混音处理后的数据(PCM,一般输出到播放设备)
         **/        
        void OnMixedAudio(const RtcAudioData& audio)  override;
        /**
         *  收到了来自某路流的视频数据(I420YUV)
         **/
        void OnVideo(const RtcVideoData& video,const std::string& streamId) override;
        /**
         *  收到了来自对端的文本消息
         **/
        void OnText(const RtcTextData& text) override;        
        /**
         * 获取当前会话的生存时间，单位为秒
         **/
        int GetAliveTime();
         /**
         * 发送音频数据，数据来源于pcm文件
         * 正常读取pcm文件的场景，每10ms读取160个样本点
         * 为了模拟tts输入的场景，也可以随机读任意个数，tts每次输入的pcm样本点可能并不是160的整数倍
         **/      
        void SendPcmData();
        /**
         * 发送视频数据，数据来源于yuv文件(I420P格式)
         **/
        void SendYuvData();
        /**
         *  音视频数据来源于MP4文件
         *  视频数据不解码直接发送，音频解码为PCM后再发送
         **/
        void SendH264Data();
        /**
         *  mp4文件中的音频
         * */
        void OnAudioFrame(const uint8_t* data,int len) override;
        /**
         *  mp4文件中的视频
         * */
        void OnVideoFrame(const uint8_t* data,int len,int width,int height) override;

        /**
         *  根据当前的网络状态以及订阅方的解码反馈，实时控制本地编码器的行为
         **/
        void OnKeyFrameRequest() override;
        void OnEncoderQosRequest(const MRtcEncoderQosParams& params) override;

        /**
         *  有新的用户加入房间，有的业务场景希望用户加入房间以后再启动录制
         *  否则，用户迟迟不加入房间，录制文件的前面10来秒都是没有声音的
         * */
        void OnNewJoiner(UserInfo user) override;   
        
        /**
         *  有人离开房间
         **/
        void OnUserGone(UserInfo user) override;

    private:
        /**
         *  启动录制
         **/       
        void StartRecord();

    private:
        MRtcEngine* engine;
        MRtcSession* session;
        int createTime;
        std::string roomId;
        std::string token;
        bool creator;
        bool doPub;
        bool doSub;
        bool mocktts;
        bool enableVideo;
        bool enableAudio;
        bool isP2P;
        int uidIndex;
        std::string localPubStreamId;
        AVSourceConfig sourceConfig;
        std::unique_ptr<PCMFile> pcmFileWriter;
        std::unique_ptr<PCMFile> pcmFileReader;
        std::unique_ptr<YuvFile> yuvFileWriter;
        std::unique_ptr<YuvFile> yuvFileReader;
        FILE* h264File;
        std::atomic_bool quit;
        std::atomic_bool peerReady;
        pthread_t audioThread;
        pthread_t videoThread;
        std::chrono::high_resolution_clock clock;
        std::chrono::high_resolution_clock::time_point lastAudioTime;
        std::chrono::high_resolution_clock::time_point lastVideoTime;
    };

    struct Demo: public MRtcEngineListener {
        Demo(const MRtcEngineInitParam& param,int maxSessions = 10);
        virtual ~Demo();

        /**
         *  监听引擎相关的事件，主要是和房间服务器的长连接保持情况，以及引擎初始化结果
         * */
        void OnEngineEvent(RtcEvent event) override ;
        /**
         *  启动一个新的会话,指定会话存活时间(秒)，以及是否做发布以及订阅操作
         *  如果roomId为空，那么会自动新建房间，否则会加入roomId指定的房间里面
         *  发布的音视频流通过config参数去指定其来源所在的文件
         *  如果指定了视频文件类型为MP4,那么音频相关的配置会被忽略掉，默认认为音频也来源这个MP4文件
         *  doPub 控制是否做发布操作，把音视频流发布到视频服务器上
         *  doSub 控制是否做订阅操作，订阅来自视频服务器上的音视频流，录制到本地文件中
         *  mocktts 控制是否模拟tts-pcm输入行为，tts的pcm输入是pcm样本点个数每次都不一样
         **/
        void FireNewSession(const std::string& roomId,
                                                    const std::string& token,
                                                    const AVSourceConfig& config,
                                                    int lifeTime = 60,
                                                    bool doPub=true,
                                                    bool doSub=true,
                                                    bool mocktts=false,
                                                    bool enableVideo=true,
                                                    bool enableAudio=true,
                                                    bool enableP2P=false);

    private:
        int uidIndex;
        int ridIndex;
        MRtcEngine* engine;
        std::atomic_bool initOK;
        std::vector<std::unique_ptr<LinuxSession>> sessions;
        int maxSessions;
    };
}
#endif