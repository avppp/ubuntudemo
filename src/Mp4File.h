#ifndef MrtcMp4File_H
#define MrtcMp4File_H
#include <string>
#ifdef __cplusplus
extern "C"
{
#endif
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavdevice/avdevice.h>
#include<libswresample/swresample.h>
#ifdef __cplusplus
};
#endif
#include <mutex>
#include <cstdlib>

namespace mrtclinux {
    class AvDataCallback {
public:
        AvDataCallback() {};
        virtual ~AvDataCallback(){};
        virtual void OnAudioFrame(const uint8_t* data,int len) = 0;
        virtual void OnVideoFrame(const uint8_t* data,int len,int width,int height) = 0;        
    };
    class Mp4Parse {
public:
        Mp4Parse(const std::string& mp4File,AvDataCallback* callback);
        int Init();
        void Start();
        void Stop();
        virtual ~Mp4Parse();
private:
        void SetOutputPcmParams(AVCodecContext* inCodecCtx);
private:
        mutable std::mutex mutex_;
        FILE* h264_;
        FILE* bsf_;
        std::string mp4File_;
        AvDataCallback* callback_;
        AVFormatContext *formatCtx_ ;
        AVCodecContext *audioCodecCtx_;
        AVCodec* audioCodec_;
        AVCodecContext* videoCodecCtx_;
        AVCodec* videoCodec_;
        struct SwrContext *audioSwrCtx_;
        int outAudioSampleSize_;
        AVBSFContext* h264bsfc_;
        int videoStreamIdx_;
        int audioStreamIdx_;
        int videoWidth_;
        int videoHeight_;
        int videoFps_; 
        int64_t frameIntervelNanos_;
        bool looping_;
        bool stopping_;       
    };
}
#endif