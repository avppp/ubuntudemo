#include "PCMFile.h"

#include <ctype.h>
#include <stdio.h>
#include <string.h>

namespace mrtclinux {

#define MAX_FILE_NAME_LENGTH_BYTE 500

PCMFile::PCMFile()
    : pcm_file_(NULL),
      samples_10ms_(160),
      frequency_(16000),
      end_of_file_(false),
      auto_rewind_(false),
      rewinded_(false),
      read_stereo_(false),
      save_stereo_(false) {
  timestamp_ =
      (((uint32_t)rand() & 0x0000FFFF) << 16) | ((uint32_t)rand() & 0x0000FFFF);
}

PCMFile::PCMFile(uint32_t timestamp)
    : pcm_file_(NULL),
      samples_10ms_(160),
      frequency_(16000),
      end_of_file_(false),
      auto_rewind_(false),
      rewinded_(false),
      read_stereo_(false),
      save_stereo_(false) {
  timestamp_ = timestamp;
}

PCMFile::~PCMFile() {
  if (pcm_file_) {
    fclose(pcm_file_);
  }
}

void PCMFile::Open(const std::string& file_name,
                   uint16_t frequency,
                   uint32_t channels,
                   const char* mode,
                   bool auto_rewind) {
  if ((pcm_file_ = fopen(file_name.c_str(), mode)) == NULL) {
    printf("Cannot open file %s.\n", file_name.c_str());
    return;
  }
  frequency_ = frequency;
  samples_10ms_ = (uint16_t)(frequency_ / 100) * channels;
  auto_rewind_ = auto_rewind;
  end_of_file_ = false;
  rewinded_ = false;
}

int32_t PCMFile::SamplingFrequency() const {
  return frequency_;
}

uint16_t PCMFile::PayloadLength10Ms() const {
  return samples_10ms_;
}

void PCMFile::Write10MsData(const int16_t* playout_buffer,
                            size_t length_smpls) {
  if (fwrite(playout_buffer, sizeof(uint16_t), length_smpls, pcm_file_) !=
      length_smpls) {
    return;
  }
}

bool PCMFile::Read10MsData(int16_t* record_buffer, size_t length_smpls) {
  bool endOfFile = false;
  if (fread(record_buffer, sizeof(uint16_t), length_smpls, pcm_file_) !=
      length_smpls) {
        endOfFile = feof(pcm_file_);
        if(auto_rewind_ && endOfFile) {
          printf("Read10MsData rewind 1\n");
          rewind(pcm_file_);
          fread(record_buffer, sizeof(uint16_t), length_smpls, pcm_file_);
        }
  } else {
    endOfFile = feof(pcm_file_);
    if(auto_rewind_ && endOfFile) {
      printf("Read10MsData rewind 2\n");
      rewind(pcm_file_);
    }
  }
  return endOfFile;
}

void PCMFile::Close() {
  fclose(pcm_file_);
  pcm_file_ = NULL;
  blocks_read_ = 0;
}

void PCMFile::FastForward(int num_10ms_blocks) {
  const int channels = read_stereo_ ? 2 : 1;
  long num_bytes_to_move =
      num_10ms_blocks * sizeof(int16_t) * samples_10ms_ * channels;
  int error = fseek(pcm_file_, num_bytes_to_move, SEEK_CUR);
  if(error != 0) {
    printf("FastForward error:%d\n", error);
  }
}

void PCMFile::Rewind() {
  rewind(pcm_file_);
  end_of_file_ = false;
  blocks_read_ = 0;
}

bool PCMFile::Rewinded() {
  return rewinded_;
}

void PCMFile::SaveStereo(bool is_stereo) {
  save_stereo_ = is_stereo;
}

void PCMFile::ReadStereo(bool is_stereo) {
  read_stereo_ = is_stereo;
}

void PCMFile::SetNum10MsBlocksToRead(int value) {
  num_10ms_blocks_to_read_ = value;
}

}  // namespace webrtc
