#include "demo.h"
#include <ctime>
#include <algorithm>
namespace mrtclinux {
    void* PcmThread(void* data) {
        LinuxSession* session = (LinuxSession*)data;
        session->SendPcmData();
    }

    void* YuvThread(void* data) {
         LinuxSession* session = (LinuxSession*)data;
        session->SendYuvData();       
    }

    void* H264Thread(void* data) {
        LinuxSession* session = (LinuxSession*)data;
        session->SendH264Data();         
    }

    LinuxSession::LinuxSession(MRtcEngine* engine,
                                                             const std::string& roomId,
                                                             const std::string& token,
                                                             const AVSourceConfig& config,
                                                             bool doPublish,
                                                             bool doSubscribe,
                                                             bool mocktts,
                                                             bool enableVideo,
                                                             bool enableAudio): sourceConfig(config) {
        this->roomId = roomId;
        this->token = token;
        this->creator = (roomId == "");
        this->doPub = doPublish;
        this->doSub = doSubscribe;
        this->mocktts = mocktts;
        this->enableVideo = enableVideo;
        this->enableAudio = enableAudio;
        this->engine = engine;
        createTime = std::time(0);
        audioThread = 0;
        videoThread = 0;
        quit = false;
        peerReady = false;
        h264File = nullptr;
    }

    LinuxSession::~LinuxSession() {
        int leftTime = std::time(0);
        std::cout<<"Destroy Session,index:"<< uidIndex << ",live for " << (leftTime-createTime) << std::endl;
        quit = true;
        if(audioThread != 0) {
            pthread_join(audioThread,nullptr);
            audioThread = 0;
        }
        if(videoThread !=0) {
            pthread_join(videoThread,nullptr);
            videoThread = 0;            
        }
        if(h264File) {
            fclose(h264File);
            h264File = nullptr;
        }
        if(session) {
            engine->DestroySession(session);
            session = nullptr;
        }
    }

    void LinuxSession::Start(int uidIndex,int ridIndex,bool enableP2P) {
        this->uidIndex = uidIndex;
        this->isP2P = enableP2P;
        session = engine->CreateSession(this);
        if(!session) {
            std::cout<<"Create session failed for " << uidIndex <<std::endl;
            return;
        }
        if(creator) {
            std::cout<<"Engine Init OK,Ready to create room"<<std::endl;
            CreatRoomParam param;
            param.autoSubscribe = false;
            param.bizName = "demo";
            param.subBiz = "default";
            param.sign = "signature";
            param.uid = std::to_string(uidIndex);
            param.workspaceId = "default";
            param.engine = ENGINE_ALIPAY;
            if(isP2P){
              param.engine = ENGINE_P2P;
            }
            param.ext = R"({"defaultRecord":false,"recordStrongDepend":false})";
            session->CreateRoom(param);
        } else {
            std::cout<<"Engine Init OK,Ready to join room " << roomId <<std::endl;
            JoinRoomParam param;
            param.autoSubscribe = false;
            param.bizName = "demo";
            param.subBiz = "default";
            param.sign = "signature";
            param.token = token;
            param.roomId = roomId;
            param.uid = std::to_string(uidIndex);
            param.workspaceId = "default";
            param.engine = ENGINE_ALIPAY;
            if(isP2P){
              param.engine = ENGINE_P2P;
            }
            session->JoinRoom(param);                        
        }       
    }

     Demo::Demo(const MRtcEngineInitParam& param,int maxSessions) {
        uidIndex = 0;
        ridIndex = 0;
        initOK = false;
        this->maxSessions = maxSessions;
        engine = MRtcEngine::Create(this);
        engine->Init(param);
     }
     //明确销毁session，停止喂数据的线程，否则engine销毁后，底层sesssion为空了，继续喂数据会core
     //总体原则是先停线程，再销毁底层Session
     Demo::~Demo() {
        sessions.clear();
        engine->Destroy();
    }

    void Demo::OnEngineEvent(RtcEvent event) {
        if (event.type == INIT) {
            if (event.code == 0) {
                initOK = true;
            } else {
                std::cout<<"Engine Init Fail,code:"<<event.code<<std::endl;
            }
        } else if(event.type == WSS_LINK) {   //通话过程中，和房间服务器的信令通道断连或者重连成功的通知
            if (event.code == 0) {
                std::cout<<"WSS_LINK OK!"<<std::endl;
            } else {
                std::cout<<"WSS_LINK Fail,code:"<<event.code<<std::endl;
            }                
        }
    }

    void Demo::FireNewSession(const std::string& roomId, 
                                const std::string& token,
                                const AVSourceConfig& config,
                                int lifeTime,
                                bool doPub,
                                bool doSub,
                                bool mocktts,
                                bool enableVideo,
                                bool enableAudio,
                                bool enableP2P) {
        if(initOK.load()) {
            sessions.erase(std::remove_if(sessions.begin(),sessions.end(),
                [&](const std::unique_ptr<LinuxSession> &s) {
                    int aliveTime = s->GetAliveTime();
                    if( aliveTime > lifeTime ) {
                        return true;
                    } else {
                        return false;
                    }
            }),sessions.end());

            if(sessions.size() >= maxSessions) {
                std::cout << "Max Session reached: " << maxSessions << std::endl;
                return;
            }
            ::srand((unsigned)time(NULL));
            uidIndex=::rand();
            std::cout << "Create Session index: " << uidIndex << std::endl;
            auto session = std::make_unique<LinuxSession>(engine,roomId,token,config,doPub,doSub,mocktts,enableVideo,enableAudio);
            session->Start(uidIndex,ridIndex++,enableP2P);
            sessions.push_back(std::move(session));
        } else {
            std::cout << "Still waiting for engine init ok" << std::endl;
        }
    }

    void LinuxSession::OnSessionEvent(RtcEvent event) {
        if (event.type == CREATE_ROOM) {
            if (event.code == 0) {
                std::cout<<"CREATE_ROOM OK,roomId and token:" << event.ext <<std::endl;
                if(doPub && !isP2P) {
                    PublishParam param;
                    param.enableAudio = true;
                    param.enableVideo = true;
                    std::cout<<"Do publish" << std::endl;
                    session->Publish(param);
                }
               StartRecord();
            } else {
                std::cout<<"CREATE_ROOM FAIL,code:"<<event.code<<std::endl;
            }
        } else if (event.type == JOIN_ROOM) {
            if (event.code == 0) {
                std::cout<<"JOIN_ROOM OK,Begin Do publish"<<std::endl;
                if(doPub) {
                    PublishParam param;
                    param.enableAudio = enableAudio;
                    param.enableVideo = enableVideo;
                    session->Publish(param);
                }
                StartRecord();
            } else {
                std::cout<<"JOIN_ROOM FAIL,code:"<<event.code<<std::endl;
            }
        } else if (event.type == PUBLISH) {
            if (event.code == 0) {
                localPubStreamId = event.ext;
                if(sourceConfig.videoType != AV_FILE_H264) {
                    std::string pcmFileName = sourceConfig.audioFile;
                    pcmFileReader.reset(new PCMFile());
                    pcmFileReader->Open(pcmFileName,sourceConfig.audioSampleRate,sourceConfig.audioChannels,"rb");
                    std::string yuvFileName = sourceConfig.videoFile;
                    yuvFileReader.reset(new YuvFile());
                    yuvFileReader->Open(yuvFileName,sourceConfig.yuvWidth,sourceConfig.yuvHeight,"rb"); 
                }             
                std::cout<<"PUBLISH OK"<<std::endl;                
            } else {
                std::cout<<"PUBLISH FAIL,code:"<<event.code<<std::endl;
            }
        } else if (event.type == SUBSCRIBE) {
            if (event.code == 0) {
                std::string pcmFileName = event.ext + ".pcm";
                pcmFileWriter.reset(new PCMFile());
                pcmFileWriter->Open(pcmFileName,16000,1,"wb+");
                if(sourceConfig.videoType == AV_FILE_YUV) {
                    std::string yuvFileName = event.ext + ".yuv";
                    yuvFileWriter.reset(new YuvFile());
                    yuvFileWriter->Open(yuvFileName,"wb+");
                } else if(sourceConfig.videoType == AV_FILE_H264) {
                    std::string h264FileName = event.ext + ".264";
                    h264File = fopen(h264FileName.c_str(),"wb+");
                }
                std::cout<<"SUBSCRIBE OK,Begin to recv data"<<std::endl;
            } else {
                std::cout<<"SUBSCRIBE FAIL,code:"<<event.code<<std::endl;
            }
        } else if (event.type == ICE_LINK) {
            if(event.code == 0) {
                if(localPubStreamId == event.ext) {
                     std::cout<<"ICE OK,Begin to send data"<<std::endl;
                     if(sourceConfig.videoType != AV_FILE_H264) {
                        if(audioThread == 0 && enableAudio) {
                            pthread_create((pthread_t*)&audioThread,(const pthread_attr_t*)NULL,PcmThread,(void*)this);
                        }
                     }
                      if(videoThread == 0 && enableVideo) {
                          if(sourceConfig.videoType == AV_FILE_YUV) {
                               pthread_create((pthread_t*)&videoThread,(const pthread_attr_t*)NULL,YuvThread,(void*)this);
                          } else if(sourceConfig.videoType == AV_FILE_H264) {
                               pthread_create((pthread_t*)&videoThread,(const pthread_attr_t*)NULL,H264Thread,(void*)this);
                          }
                     }                    
                }
            } else {
                std::cout<<"ICE_LINK FAIL,code:"<<event.code<<std::endl;
            }
        } else if (event.type == EXIT_ROOM) {
            if(session) {
                engine->DestroySession(session);
                session = nullptr;
            }
            if (event.code == 0) {
                std::cout<<"EXIT_ROOM OK"<<std::endl;
            } else {
                std::cout<<"EXIT_ROOM FAIL,code:"<<event.code<<std::endl;
            }            
        } else if (event.type == START_RECORD) {
            if (event.code == 0) {
                std::cout<<"Start record OK"<<std::endl;
            } else {
                std::cout<<"Start record FAIL"<<std::endl;
            } 
        }
    }

    /**
     *  通知上层房间里面有新人发布的流
     *  如果创建房间或者进入房间设置了自动订阅，那么底层会自动订阅该新人的流
     *  否则需要在该函数里面手动订阅
     **/
    void LinuxSession::OnNewPublisher(StreamInfo info) {
        std::cout << "new stream pub,id is " << info.id << ",pub by user " << info.uid<<std::endl;
          if(doSub) {
            SubscribeParam param;
            param.enableAudio = true;
            param.enableVideo = true;
            param.streamId = info.id;
            session->Subscribe(param);
            session->SendText("I'm  watching send use websocket",{info.uid});
        }
    }

    /**
     *  通知上层房间里面有人订阅了某个流
     *  如果识别到别人订阅己方发布的流成功，也就是意味着别人做好了接收流数据的准备,那么开始发送流数据
     **/
    void LinuxSession::OnNewSubscriber(StreamInfo info) {
        std::cout << "new stream sub,id is " << info.id << ",sub by user " << info.uid<<std::endl;
        if(info.id == localPubStreamId) {   
            //有时候业务希望另一方准备好接收数据了，再开始推送音视频流到房间里面
            peerReady = true;
        }
    }

    /**
     *  通知上层房间里面有某路流停止了发布
     *  如果是两人的房间，那么相当于对方退出了，我方这个时候也可以退出
     *  如果是多人的房间，那么相当于某个人退出了，我方可以退出也可以不退出，继续接收房间里面其他人的流
     **/
    void LinuxSession::OnStreamGone(StreamInfo info) {
        std::cout << "stream gone,id is " << info.id << ",from user " << info.uid<<std::endl;
        quit = true;
        if(session) {
            engine->DestroySession(session);
            session = nullptr;
        }
    }

    void LinuxSession::OnUserGone(UserInfo info) {
        std::cout << "user left room,id is " << info.uid <<std::endl;
    }

    /**
     *  收到了来自某路流的音频数据(PCM)
     **/        
    void LinuxSession::OnAudio(const RtcAudioData& audio,const std::string& streamId) {
        std::cout << "OnAudio[" << streamId << "]len:" << audio.length << ",ts:" << audio.timestamp << std::endl;
        pcmFileWriter->Write10MsData((const int16_t*)(audio.data),audio.length/2);
    }

     void LinuxSession::OnMixedAudio(const RtcAudioData& audio) {
         std::cout << "OnMixAudio,len:" << audio.length << ",ts:" << audio.timestamp << std::endl;
     }

    /**
     *  收到了来自某路流的视频数据(I420YUV)
     **/
    void LinuxSession::OnVideo(const RtcVideoData& video,const std::string& streamId) {
        if(sourceConfig.videoType == AV_FILE_YUV) {
            std::cout << "OnVideoFrame[" << streamId << "]width:" << video.width <<",height:" << video.height
                    << ",len:" << video.length << ",ts:" << video.timestamp << std::endl;
            yuvFileWriter->WriteFrame(video);
        } else if(sourceConfig.videoType == AV_FILE_H264) {
            std::cout << "OnVideoH264[" << streamId << "]len:" << video.length << std::endl;
            fwrite(video.dataY,1,video.length,h264File);
            fflush(h264File);
        }
    }

    /**
     *  收到了来自对端的文本消息
     **/
    void LinuxSession::OnText(const RtcTextData& text){
        std::cout << "PEER_MSG from " << text.peer << ",msg:" << text.msg << std::endl;
    }

    int LinuxSession::GetAliveTime() {
        if(quit.load()){
          return INT_MAX;
        }
        int nowTime = std::time(0);
        return nowTime - createTime;
    }

    void LinuxSession::SendPcmData() {
        int32_t ttsFactor = 5;
        int32_t normalSmplNum = pcmFileReader->PayloadLength10Ms();
        int16_t *buffer = new int16_t[normalSmplNum*ttsFactor];
        RtcAudioData audio;
        audio.data=(uint8_t*)buffer;
        audio.length=normalSmplNum*2;
        audio.timestamp=-1;
        int32_t random = 0;
        int32_t interval = 10;
        int32_t realSmplNum = normalSmplNum;
        ::srand((unsigned)time(NULL));
        int32_t dataSeq = 0;
        while(!quit.load()) {
            if(creator && !peerReady.load()) {
                usleep(5*1000);
                continue;
            }
           lastAudioTime = clock.now();
           if(mocktts){
               random = ::rand() % ttsFactor;
               if(random > 0) {
                    realSmplNum = normalSmplNum*random + random;
                    audio.length = realSmplNum*2;
                    //interval *= random;
               }
           }
           bool endOfFile = pcmFileReader->Read10MsData(buffer,realSmplNum);
           session->FeedAudio(audio);
           std::string data = "audio frame seq ";
           data += std::to_string(dataSeq++);
           session->SendData(data);
           auto ms = std::chrono::duration_cast<std::chrono::microseconds>(clock.now()-lastAudioTime);
           int32_t sleepTime = ms.count() < interval ? interval-ms.count() : interval;
           if(endOfFile){
               std::cout<<"tts answer over,let's have a rest for 15 senconds" << std::endl;
               sleep(15);
           }else {
               usleep(sleepTime*1000);
           }
        }
        std::cout<<"Audio thread quit for stream " << localPubStreamId << std::endl;
    }

    void LinuxSession::SendYuvData() {
        int fpsIntervalMs = 1000 / sourceConfig.yuvFps;
        while(!quit.load()) {
            if(creator && !peerReady.load()) {
                usleep(5*1000);
                continue;
            }
           lastAudioTime = clock.now();
           auto frame = yuvFileReader->ReadFrame();
           session->FeedVideo(frame);
           if(frame.dataY){
               delete frame.dataY;
           }
           if(frame.dataU){
               delete frame.dataU;
           }
           if(frame.dataV){
               delete frame.dataV;
           }
           auto ms = std::chrono::duration_cast<std::chrono::microseconds>(clock.now()-lastAudioTime);
           int32_t sleepTime = ms.count() < fpsIntervalMs ? fpsIntervalMs-ms.count() : fpsIntervalMs;
           usleep(sleepTime*1000);
        }
        std::cout<<"Video thread quit for stream " << localPubStreamId << std::endl;
    }

    void LinuxSession::SendH264Data() {
        std::unique_ptr<Mp4Parse> mp4 = std::make_unique<Mp4Parse>(sourceConfig.videoFile,this);
        mp4->Start();
    }

    void LinuxSession::OnAudioFrame(const uint8_t* data,int len) {
        RtcAudioData audio;
        audio.data = data;
        audio.length = len;
        session->FeedAudio(audio);
    }

    void LinuxSession::OnVideoFrame(const uint8_t* data,int len,int width,int height) {
        RtcVideoData video;
        video.dataY = data;
        video.length = len;
        video.width = width;
        video.height = height;
        session->FeedVideo(video);
    }

    void  LinuxSession::OnKeyFrameRequest() {
        std::cout << "OnKeyFrameRequest for stream " << localPubStreamId << std::endl;
    }

    void  LinuxSession::OnEncoderQosRequest(const MRtcEncoderQosParams& params) {
        std::cout << "OnEncoderQosRequest for stream " << localPubStreamId 
                          <<  ",fps " << params.target_fps << ",bps " << params.target_bps << std::endl;
    }

    //P2P场景下，因为Room目前不具备信令缓存能力，所以需要等到对端加入的时候，再做发布
    void LinuxSession::OnNewJoiner(UserInfo user) {
        std::cout << "New Joner uid " << user.uid << ",type " << user.type << std::endl;
        if(doPub && isP2P) {
            PublishParam param;
            param.enableAudio = true;
            param.enableVideo = true;
            std::cout<<"Do publish" << std::endl;
            session->Publish(param);
        }
    }

    void LinuxSession::StartRecord() {
        //P2P模式下，目前不支持录制
        if(!isP2P) {
            //录制所有人的音视频，包括用户端和Linux端
            //recordTag表示业务这次录制的意图，业务方可以自定义其含义
            //录制结束后，Room服务器会通过消息队列告诉业务方录制结果,录制结果里面包含了recordTag以及录制文件的下载地址
            //tagFilter用来过滤需要录制的流，每一条流发布的时候可以带一个tag，如果在tag中匹配到了tagFilter，那么
            //这条流就会被录制，tagFilter定义为0表示录制所有流
            std::string paramAll = R"({"recordTag":"xxx","tagFilter":"0"})";
            session->StartRecord(paramAll);

            //单独录制用户端的音视频，目前手机用户发布的流一般业务并没有自定义tag，tag采取的默认值VIDEO_SOURCE_xxx
            std::string paramUser = R"({"recordTag":"yyy","tagFilter":"VIDEO_SOURCE"})";
            session->StartRecord(paramUser);
        }
    }
}
